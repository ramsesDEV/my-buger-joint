import React from 'react';
import './App.css';
import Person from './Person/Person.js';

function App() {
  return (
    <div className="App">
      <Person name="Ramses" age="28">And my hobbies suck</Person>
      <Person name="Penelope" age="26"></Person>
    </div>
  );
}

export default App;
